import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, HashRouter, Route, Switch } from "react-router-dom";
import blogdata from './data/blogdata.json';
import Singleblogdata from './data/single-blogdata.json';
import Accueil from './components/accueil';
import RechercheFormation from './components/recherche-formation';
import RechercheLogement from './components/recherche-logement';
import RechercheCommunaute from './components/recherche-communaute';
import HomeV2 from './components/home-v2';
import HomeV3 from './components/home-v3';
import HomeV4 from './components/home-v4';
import VieEtudiante from './components/vie-etudiante';
import SeLoger from './components/se-loger';
import ServiceAlimentaire from './components/service-alimentaire';

import PropertyDetails from './components/property-details';
import Etudier from './components/etudier';
import Programmes from './components/programmes';
import Pricing from './components/pricing';
import UserList from './components/user-list';
import Registraion from './components/registration';
import Error from './components/error';
import Faq from './components/faq';
import News from './components/news';
import NewsDetails from './components/news-details';
import Contact from './components/contact';
import SearchMap from './components/search-map';
import CoutBourse from './components/cout-bourse';
import Admission from './components/admission';
import AddNew from './components/add-property';
import Dashbord from './components/dashbord';

class Root extends Component {
    render() {
        return(
            <Router>
                <HashRouter basename="/">
                <div>
                <Switch>
                    <Route exact path="/" component={Accueil} />
                    <Route exact path="/recherche-formation" component={RechercheFormation} />
                    <Route exact path="/recherche-logement" component={RechercheLogement} />
                    <Route path="/recherche-communaute" component={RechercheCommunaute} />
                    <Route path="/home-v3" component={HomeV3} />
                    <Route path="/home-v4" component={HomeV4} />
                    <Route path="/vie-etudiante" component={VieEtudiante} />
                    <Route path="/se-loger" component={SeLoger} />
                    <Route path="/service-alimentaire" component={ServiceAlimentaire} />
                
                    <Route path="/property-details" component={PropertyDetails} />
                    <Route path="/etudier" component={Etudier} />
                    <Route path="/programmes" component={Programmes} />
                    <Route path="/pricing" component={Pricing} />
                    <Route path="/user-list" component={UserList} />
                    <Route path="/registration" component={Registraion} />
                    <Route path="/error" component={Error} />
                    <Route path="/faq" component={Faq} />
                    <Route path="/news" component={News} />
                    <Route path="/news-details" component={NewsDetails} />
                    <Route path="/contact" component={Contact} />
                    <Route path="/search-map" component={SearchMap} />
                    <Route path="/cout-bourse" component={CoutBourse} />
                    <Route path="/admission" component={Admission} />
                    <Route path="/add-property" component={AddNew} />
                    <Route path="/dashbord" component={Dashbord} />
                </Switch>
                </div>
                </HashRouter>
            </Router>
        )
    }
}

export default Root;

ReactDOM.render(<Root />, document.getElementById('realdeal'));
