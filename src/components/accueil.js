import React from 'react';
import Navbar from './global-components/navbar';
import Banner from './section-components/banner';
import Search from './section-components/search';

import Service from './section-components/service';
import Mission from './section-components/mission';
import AboutUs from './section-components/about-us';
import ServiceTwo from './section-components/service-two';
import Explore from './section-components/explore';
import FeaturedProperties from './section-components/featured-properties';
import Ads from './section-components/ads';
import PropertiesByCities from './section-components/properties-by-cities';
import RecentProperties from './section-components/recent-properties';
import FeaturedPorject from './section-components/featured-project';
import WhyChooseUs from './section-components/why-choose-us';
import OurPartner from './section-components/our-partner';
import Footer from './global-components/footer';

const Accueil = () => {
    return <div>
        <Navbar />
     
        <Banner />
        <Service />
        
        <Search />
       
       
        
        <Footer />
    </div>
}

export default Accueil

