import React from 'react';
import Navbar from './global-components/navbar';
import PageHeader from './global-components/page-header';



import Team from './section-components/team';
import Client from './section-components/client';
import Footer from './global-components/footer';

const Etudier = () => {
    return <div>
        <Navbar />
        <PageHeader headertitle="Etudier" />
       
      
        
        
        <Footer />
    </div>
}

export default Etudier

