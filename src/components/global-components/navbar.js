import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navbar extends Component {

    render() {
        let publicUrl = process.env.PUBLIC_URL+'/'
        let imgattr = 'logo'
        let anchor = '#'
        return (
            
                <div className="navbar-area">
                <nav className="navbar navbar-area navbar-expand-lg">
                  <div className="container nav-container">
                 
        
                   
                  <div className= "logos">
                   <Link to="/"><img src={publicUrl+"/assets/img/logo.png"}  alt="logo" /></Link>
                  </div>
                    
                    
                    
                    
                    
                    
                        <div className="nav-right-part nav-right-part-desktop readeal-top">
                          <Link className="btn btn-yellow" to="/registration">ACCEDER A MON COMPTE ETUDES AU SENEGAL</Link>
                        </div>
                   
                   
                  </div>
                </nav>
              </div>
            
        )
    }
}


export default Navbar