import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navbar extends Component {

    render() {
        let publicUrl = process.env.PUBLIC_URL+'/'
        let imgattr = 'logo'
        let anchor = '#'
        return (
            <div>
                <div className="navbar-area">
                <nav className="navbar navbar-area navbar-expand-lg">
                  <div className="container nav-container">
                    <div className="responsive-mobile-menu">
                      <button className="menu toggle-btn d-block d-lg-none" data-toggle="collapse" data-target="#realdeal_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="icon-left" />
                        <span className="icon-right" />
                      </button>
                    </div>
                    <div className="logo readeal-top">
                      <Link to="/"><img src={publicUrl+"/assets/img/logo.png"} alt="logo" /></Link>
                    </div>
                  
                    <div className="collapse navbar-collapse" id="realdeal_main_menu" >
                      <ul className="navbar-nav menu-open readeal-top" >
                      <li ><Link to="/#">Accueil</Link></li>
                        <li className="menu-item-has-children">
                          <a href="#">Etudier au Sénégal</a>
                          <ul className="sub-menu">
                            <li><Link to="/etudier">Tous sur les etudes au Sénégal</Link></li>
                            <li><Link to="/programmes">Programmes et cours</Link></li>
                            <li><Link to="/admission">Demande d'admission</Link></li>
                            <li><Link to="/cout-bourse">Coût des etudes, Bourses et Soutien Financier</Link></li>
                            
                          </ul>
                        </li>
                        <li className="menu-item-has-children">
                          <a href="#">Vie au Sénégal</a>
                          <ul className="sub-menu">
                             <li><Link to="/vie-etudiante">Vie etudiante</Link></li>
                            <li><Link to="/se-loger">Se loger</Link></li>
                            <li><Link to="/service-alimentaire">Service alimentaire</Link></li>
                            <li><Link to="/ma-communaute">Ma communauté</Link></li>
                            
                          </ul>
                        </li>
                        
                        <li className="menu-item-has-children">
                          <a href="#">Nos Services</a>
                          <ul className="sub-menu">
                            <li><Link to="/news">News</Link></li>
                            <li><Link to="/news-details">News Details</Link></li>
                          </ul>
                        </li>
                        
                      </ul>
                    </div>
                    
                  </div>
                </nav>
              </div>
            </div>
        )
    }
}


export default Navbar_1