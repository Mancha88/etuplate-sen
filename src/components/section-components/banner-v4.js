import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';

class BannerV4 extends Component {

    componentDidMount() {

    const $ = window.$;
    
     if ($('.single-select').length){
            $('.single-select').niceSelect();
        }
  }

    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imagealt = 'image'
        let data = sectiondata.banner

        const inlineStyle = {
            backgroundImage: 'url('+publicUrl+'/assets/img/recherche-formation.png)'
        }

    return <div className="banner-area jarallax" style={inlineStyle}>
            <div className="banner-inner-wrap">
            <div className="rechercher-logement-bar-top">
            <Link to="/"> <a className="annuaire">ANNUAIRE DES CONSULAIRE</a></Link>
      </div>
          
     
        <input className="choix-pays" type="search" id="maRecherche" name="q" placeholder="Veuillez choisir votre pays" required></input>
        
        <button className="recherche">Rechercher</button>
   
         <span class="validity"></span>
      
    </div>
  </div>

        }
}

export default BannerV4