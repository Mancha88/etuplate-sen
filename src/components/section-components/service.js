import React, { Component } from 'react';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';
import { Link } from 'react-router-dom';

class Service extends Component {


    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imagealt = 'image'
        let data = sectiondata.services


    return <div className="service">

                 <h2 className="a-propos">A propos</h2>
                 <h2 className="bourse">Bourse d'étude</h2>
                 <h2 className="decouverte">Découverte</h2>

              <div className="service-actu1">
                <img className="service-actu1-img" src={publicUrl+"/assets/img/service/2.png"} alt="logo" />
                <div className="sous-actu1">
                
                </div>
               
              </div>
              <div className="service-actu2">
              <img className="service-actu2-img" src={publicUrl+"/assets/img/service/2.png"} alt="logo" />
                <div className="sous-actu2">
                
                </div>
              </div>
              <div className="service-actu3">
              <img className="service-actu3-img" src={publicUrl+"/assets/img/service/2.png"} alt="logo" />
                <div className="sous-actu3">
                
                </div>
              </div>
      </div>


        }
}

export default Service